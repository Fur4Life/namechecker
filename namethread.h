#ifndef NAMETHREAD_H
#define NAMETHREAD_H

#include <QObject>
#include <QThread>
#include <QDebug>
#include <QEventLoop>
#include <QNetworkAccessManager>
#include <QNetworkRequest>
#include <QNetworkReply>
#include <QNetworkCookieJar>
#include <QNetworkCookie>
#include <QNetworkProxy>
#include <QVariant>
#include <QSslConfiguration>
#include <QJsonValue>
#include <QJsonDocument>
#include <QJsonObject>
#include <QJsonArray>
#include <QRunnable>
#include <QFile>

class NameThread : public QObject, public QRunnable
{
    Q_OBJECT
public:
    explicit NameThread(QObject *parent = 0);
    void doSetup(QString service, QString user);
    void appNet();
    void askFm();
    void blogger();
    void clubNintendo();
    void domains();
    void formspring();
    void kongregate();
    void lastFm();
    void leagueOfLegendsNA();
    void likes();
    void minecraft();
    void majorLeagueGaming();
    void neopets();
    void pastebin();
    void pheed();
    void reddit();
    void roblox();
    void runescape();
    void soundcloudUsernames();
    void soundcloudPermaLinks();
    void spotify();
    void steam();
    void tinychat();
    void tumblr();
    void twitch();
    void twitter();
    void umgGaming();
    void youtube();

private:
    QString Service;
    QString User;
    QByteArray USERAGENT;

signals:
    void nameWorking(QString name);
    void nameNotWorking(QString name);
    void finished();

public slots:
    void run();

};

#endif // NAMETHREAD_H
