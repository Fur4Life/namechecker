#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QThreadPool>
#include <QDir>
#include <QFileDialog>
#include <QLineEdit>
#include <QInputDialog>
#include <QJsonValue>
#include <QJsonDocument>
#include <QJsonObject>
#include <QJsonArray>
#include "namethread.h"

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();

private slots:
    void on_startButton_clicked();
    void onNameWorking(QString name);
    void onNameNotWorking(QString name);
    void on_nameButton_clicked();

    void on_servicesBox_currentIndexChanged(const QString &arg1);

private:
    Ui::MainWindow *ui;
    NameThread *name;
    QThreadPool *namePool;
    QStringList names;
};

#endif // MAINWINDOW_H
