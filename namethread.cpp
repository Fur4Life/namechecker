#include "namethread.h"

NameThread::NameThread(QObject *parent) :
    QObject(parent)
{
}

void NameThread::run()
{
    USERAGENT = "Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/40.0.2194.2 Safari/537.36";
    if (Service == "App.net"){
        this->appNet();
    } else if (Service == "Ask.fm"){
        this->askFm();
    } else if (Service == "Blogger"){
        this->blogger();
    } else if (Service == "Club Nintendo"){
        this->clubNintendo();
    } else if (Service == "Domains"){
        this->domains();
    } else if (Service == "Formspring"){
        this->formspring();
    } else if (Service == "Kongregate"){
        this->kongregate();
    } else if (Service == "Last.fm"){
        this->lastFm();
    } else if (Service == "League of Legends (NA)"){
        this->leagueOfLegendsNA();
    }
}

void NameThread::appNet()
{
    QEventLoop eventLoop;
    QNetworkAccessManager mgr;
    QObject::connect(&mgr, SIGNAL(finished(QNetworkReply*)), &eventLoop, SLOT(quit()));

    QNetworkRequest req(QUrl(QString("https://join.app.net/ajax/v1/validation/makana_username_check")));
    req.setRawHeader("Accept", "*/*");
    req.setRawHeader("User-Agent", USERAGENT);
    req.setRawHeader("X-CSRFToken", "pUke5YUUoB7OSJfh9g0YVVm7vE4zqTeF");
    req.setRawHeader("Referer", "https://join.app.net/signup");
    req.setRawHeader("Cookie", "csrftoken=pUke5YUUoB7OSJfh9g0YVVm7vE4zqTeF");
    req.setRawHeader("Content-Type", "application/x-www-form-urlencoded");

    QByteArray payload;
    payload.append("value=" + User);

    QNetworkReply *reply = mgr.post(req, payload);
    eventLoop.exec();

    if (reply->error() == QNetworkReply::NoError) {
        QString body = reply->readAll();
        QJsonObject jsonObj = QJsonDocument::fromJson(body.toUtf8()).object();
        if (jsonObj["value"].toString() == "ok") {
            emit nameWorking(User);
        } else {
            emit nameNotWorking(User);
        }
        delete reply;
    } else {
        qDebug() << "Failure" << reply->errorString();
        delete reply;
    }
    emit finished();
    return;
}

void NameThread::askFm()
{
    QEventLoop eventLoop;
    QNetworkAccessManager mgr;
    QObject::connect(&mgr, SIGNAL(finished(QNetworkReply*)), &eventLoop, SLOT(quit()));

    QNetworkRequest req(QUrl(QString("http://ask.fm/users/check_username?login=" + User)));
    req.setRawHeader("Accept", "*/*");
    req.setRawHeader("User-Agent", USERAGENT);

    QNetworkReply *reply = mgr.get(req);
    eventLoop.exec();

    if (reply->error() == QNetworkReply::NoError) {
        QString body = reply->readAll();
        QJsonObject jsonObj = QJsonDocument::fromJson(body.toUtf8()).object();
        if (jsonObj["error"].toString() == NULL) {
            emit nameWorking(User);
        } else {
            emit nameNotWorking(User);
        }
        delete reply;
    } else {
        qDebug() << "Failure" << reply->errorString();
        delete reply;
    }
    emit finished();
    return;
}

void NameThread::blogger()
{
    QEventLoop eventLoop;
    QNetworkAccessManager mgr;
    QObject::connect(&mgr, SIGNAL(finished(QNetworkReply*)), &eventLoop, SLOT(quit()));

    QNetworkRequest req(QUrl(QString("http://" + User + ".blogspot.com/")));
    req.setRawHeader("Accept", "*/*");
    req.setRawHeader("User-Agent", USERAGENT);

    QNetworkReply *reply = mgr.get(req);
    eventLoop.exec();

    if (reply->error() == QNetworkReply::ContentNotFoundError) {
        emit nameWorking(User);
        delete reply;
    } else {
        emit nameNotWorking(User);
        delete reply;
    }
    emit finished();
    return;
}

void NameThread::clubNintendo()
{
    QEventLoop eventLoop;
    QNetworkAccessManager mgr;
    QObject::connect(&mgr, SIGNAL(finished(QNetworkReply*)), &eventLoop, SLOT(quit()));

    QNetworkRequest req(QUrl(QString("https://club.nintendo.com/api/account/validate/username")));
    req.setRawHeader("Accept", "*/*");
    req.setRawHeader("User-Agent", USERAGENT);
    req.setRawHeader("Referer", "https://club.nintendo.com/registration.do;jsessionid=D5D2FFF0C67EC377834FDEA725ACA03D");
    req.setRawHeader("Content-Type", "application/x-www-form-urlencoded");

    QByteArray payload;
    payload.append("username=" + User);

    QNetworkReply *reply = mgr.post(req, payload);
    eventLoop.exec();

    if (reply->error() == QNetworkReply::NoError) {
        QString body = reply->readAll();
        QJsonObject jsonObj = QJsonDocument::fromJson(body.toUtf8()).object();
        if (jsonObj["isValid"].toBool()) {
            emit nameWorking(User);
        } else {
            emit nameNotWorking(User);
        }
        delete reply;
    } else {
        qDebug() << "Failure" << reply->errorString();
        delete reply;
    }
    emit finished();
    return;
}

void NameThread::domains()
{
    QEventLoop eventLoop;
    QNetworkAccessManager mgr;
    QObject::connect(&mgr, SIGNAL(finished(QNetworkReply*)), &eventLoop, SLOT(quit()));

    QNetworkRequest req(QUrl(QString("http://freedomainapi.com/?key=kwgvk2dlnl&domain=" + User)));
    req.setRawHeader("Accept", "*/*");
    req.setRawHeader("User-Agent", USERAGENT);

    QNetworkReply *reply = mgr.get(req);
    eventLoop.exec();

    if (reply->error() == QNetworkReply::NoError) {
        QString body = reply->readAll();
        QJsonObject jsonObj = QJsonDocument::fromJson(body.toUtf8()).object();
        if (jsonObj["available"].toBool()) {
            emit nameWorking(User);
        } else {
            emit nameNotWorking(User);
        }
        delete reply;
    } else {
        qDebug() << "Failure" << reply->errorString();
        delete reply;
    }
    emit finished();
    return;
}

void NameThread::formspring()
{
    QEventLoop eventLoop;
    QNetworkAccessManager mgr;
    QObject::connect(&mgr, SIGNAL(finished(QNetworkReply*)), &eventLoop, SLOT(quit()));

    QNetworkRequest req(QUrl(QString("https://api.spring.me/register/checkusername/")));
    req.setRawHeader("Accept", "application/json, text/plain, */*");
    req.setRawHeader("User-Agent", USERAGENT);
    req.setRawHeader("Referer", "http://new.spring.me/");
    req.setRawHeader("Content-Type", "application/x-www-form-urlencoded");

    QByteArray payload;
    payload.append("username=" + User);

    QNetworkReply *reply = mgr.post(req, payload);
    eventLoop.exec();

    if (reply->error() == QNetworkReply::NoError) {
        QString body = reply->readAll();
        QJsonObject jsonObj = QJsonDocument::fromJson(body.toUtf8()).object();
        if (jsonObj["status"].toString() == "ok") {
            emit nameWorking(User);
        } else {
            emit nameNotWorking(User);
        }
        delete reply;
    } else if (reply->error() == QNetworkReply::ContentConflictError) {
        emit nameNotWorking(User);
        delete reply;
    }
    emit finished();
    return;
}

void NameThread::kongregate()
{
    QEventLoop eventLoop;
    QNetworkAccessManager mgr;
    QObject::connect(&mgr, SIGNAL(finished(QNetworkReply*)), &eventLoop, SLOT(quit()));

    QNetworkRequest req(QUrl(QString("http://www.kongregate.com/accounts/availability?username=" + User)));
    req.setRawHeader("Accept", "*/*");
    req.setRawHeader("User-Agent", USERAGENT);
    req.setRawHeader("Cookie", "_kongregate_session=BAh7CkkiD3Nlc3Npb25faWQGOgZFVEkiJTkyYjJmNGRlM2FhZDQxY2EyZjJkMzAxYzQ3NzJlYjA1BjsAVEkiCWluaXQGOwBGVEkiD3RyYW5zbGF0b3IGOwBGRkkiEF9jc3JmX3Rva2VuBjsARkkiMTN6VDZ4clpXa0VTVVVpSisrM0dpSG1hMjYwbDNGOHE5eUJrZ1EvR0s1Z0E9BjsARkkiEW9yaWdpbmFsX3VyaQY7AEYiH2h0dHA6Ly93d3cua29uZ3JlZ2F0ZS5jb20v--e1aa5571a9267b6e76f44542e7c2d7e3f8a82dc1");

    QNetworkReply *reply = mgr.get(req);
    eventLoop.exec();

    if (reply->error() == QNetworkReply::NoError) {
        QString body = reply->readAll();
        QJsonObject jsonObj = QJsonDocument::fromJson(body.toUtf8()).object();
        if (jsonObj["success"].toBool()) {
            emit nameWorking(User);
        } else {
            emit nameNotWorking(User);
        }
        delete reply;
    } else if (reply->error() == QNetworkReply::ContentConflictError) {
        emit nameNotWorking(User);
        delete reply;
    }
    emit finished();
    return;
}

void NameThread::lastFm()
{
    QEventLoop eventLoop;
    QNetworkAccessManager mgr;
    QObject::connect(&mgr, SIGNAL(finished(QNetworkReply*)), &eventLoop, SLOT(quit()));

    QNetworkRequest req(QUrl(QString("http://www.last.fm/ajax/nametaken?username=" + User)));
    req.setRawHeader("Accept", "*/*");
    req.setRawHeader("User-Agent", USERAGENT);

    QNetworkReply *reply = mgr.get(req);
    eventLoop.exec();

    if (reply->error() == QNetworkReply::NoError) {
        QString body = reply->readAll();
        if (body == "true") {
            emit nameWorking(User);
        } else {
            emit nameNotWorking(User);
        }
        delete reply;
    } else {
        qDebug() << "Failure" << reply->errorString();
        delete reply;
    }
    emit finished();
    return;
}

void NameThread::leagueOfLegendsNA()
{
    QEventLoop eventLoop;
    QNetworkAccessManager mgr;
    QObject::connect(&mgr, SIGNAL(finished(QNetworkReply*)), &eventLoop, SLOT(quit()));

    QFile league("LeagueAPIKey.json");
    league.open(QIODevice::ReadOnly);
    QJsonObject jsonObj = QJsonDocument::fromJson(league.readAll()).object();

    QNetworkRequest req(QUrl(QString("https://na.api.pvp.net/api/lol/na/v1.4/summoner/by-name/" + User + "?api_key=" + jsonObj["Key"].toString())));
    req.setRawHeader("Accept", "*/*");
    req.setRawHeader("User-Agent", USERAGENT);

    QNetworkReply *reply = mgr.get(req);
    eventLoop.exec();

    if (reply->error() == QNetworkReply::ContentNotFoundError) {
        emit nameWorking(User);
        delete reply;
    } else {
        emit nameNotWorking(User);
        delete reply;
    }
    emit finished();
    return;
}

void NameThread::likes()
{

}

void NameThread::minecraft()
{

}

void NameThread::majorLeagueGaming()
{

}

void NameThread::neopets()
{

}

void NameThread::pastebin()
{

}

void NameThread::pheed()
{

}

void NameThread::reddit()
{

}

void NameThread::roblox()
{

}

void NameThread::runescape()
{

}

void NameThread::soundcloudUsernames()
{

}

void NameThread::soundcloudPermaLinks()
{

}

void NameThread::spotify()
{

}

void NameThread::steam()
{

}

void NameThread::tinychat()
{

}

void NameThread::tumblr()
{

}

void NameThread::twitch()
{

}

void NameThread::twitter()
{

}

void NameThread::umgGaming()
{

}

void NameThread::youtube()
{

}

void NameThread::doSetup(QString service, QString user)
{
    Service = service;
    User = user;
}
