#include "mainwindow.h"
#include "ui_mainwindow.h"

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);

    QStringList services;
    services << "App.net"
             << "Ask.fm"
             << "Blogger"
             << "Club Nintendo"
             << "Domains"
             << "Formspring"
             << "Kongregate"
             << "Last.fm"
             << "League of Legends (NA)"
             << "Likes"
             << "Minecraft"
             << "Major League Gaming"
             << "Neopets"
             << "Pastebin"
             << "Pheed"
             << "Reddit"
             << "Roblox"
             << "Runescape"
             << "Soundcloud (Usernames)"
             << "Soundcloud (PermaLinks)"
             << "Spotify"
             << "Steam"
             << "Tinychat"
             << "Tumblr"
             << "Twitch"
             << "Twitter"
             << "UMGGaming"
             << "YouTube";

    ui->servicesBox->addItems(services);
}

MainWindow::~MainWindow()
{
    delete ui;
    delete name;
    delete namePool;
}

void MainWindow::onNameWorking(QString name)
{
    this->ui->workingList->addItem(new QListWidgetItem(name, this->ui->workingList));
}

void MainWindow::onNameNotWorking(QString name)
{
    this->ui->notWorkingList->addItem(new QListWidgetItem(name, this->ui->notWorkingList));
}


void MainWindow::on_startButton_clicked()
{

    namePool = QThreadPool::globalInstance();

    namePool->setMaxThreadCount(this->ui->spinBox->text().toInt());

    foreach (QString str, names) {
        name = new NameThread();

        connect(name, SIGNAL(nameWorking(QString)), this, SLOT(onNameWorking(QString)));
        connect(name, SIGNAL(nameNotWorking(QString)), this, SLOT(onNameNotWorking(QString)));
        name->doSetup(ui->servicesBox->currentText(), str);
        namePool->start(name);
        if (ui->servicesBox->currentText() == "League of Legends (NA)") {
            QTime dieTime = QTime::currentTime().addMSecs(1250);
            while( QTime::currentTime() < dieTime )
            {
                QCoreApplication::processEvents( QEventLoop::AllEvents, 100 );
            }
        }
    }
    namePool->waitForDone();
}

void MainWindow::on_nameButton_clicked()
{
    QString fileName = QFileDialog::getOpenFileName(this, "Open Name List", "", "Text Files (*.txt)");
    QFile TextFile(fileName);

    QTextStream textStream(&TextFile);
    if(!TextFile.open(QIODevice::ReadOnly)) {
          qDebug() << "Couldn't Read Username File!";
     }
    while (true)
    {
        QString line = textStream.readLine();
        if (line.isNull()) {
            break;
        } else {
            names.append(line);
        }
    }
}

void MainWindow::on_servicesBox_currentIndexChanged(const QString &arg1)
{
    if (arg1 == "League of Legends (NA)") {
        this->ui->spinBox->setValue(1);
        this->ui->spinBox->setEnabled(false);
        bool ok;
        QFile league("LeagueAPIKey.json");
        if(!league.exists()){
            QString apikey = QInputDialog::getText(this, tr("League of Legends API Key"),
                                                   tr("League of Legends API Key (https://developer.riotgames.com/):"),
                                                   QLineEdit::Normal,
                                                   "", &ok);
            if (ok && !apikey.isEmpty()) {
                league.open(QIODevice::WriteOnly);
                QTextStream stream(&league);
                stream << QString("{\"Key\": \"" + apikey + "\"}");
            }
        }
    } else {
        this->ui->spinBox->setEnabled(true);
    }
}
