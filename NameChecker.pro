#-------------------------------------------------
#
# Project created by QtCreator 2014-10-20T21:49:54
#
#-------------------------------------------------

QT       += core gui network

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = NameChecker
TEMPLATE = app


SOURCES += main.cpp\
        mainwindow.cpp \
    namethread.cpp

HEADERS  += mainwindow.h \
    namethread.h

FORMS    += mainwindow.ui
